﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public void PlayGame()
    {
        Debug.Log("PlayGame");
        SceneManager.LoadScene("SpaceShooter");
    }

    public void QuitGame()
    {
        Debug.Log("QuitGame");

        Application.Quit();

    }
}