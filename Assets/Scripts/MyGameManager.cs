﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {
	public Text highscoreText;
	public Text livesText;
	private int lives;
	private int highscore;

	private static MyGameManager instance;

	void Awake(){
		if (instance == null) {
			instance = this;
		}
	}

	public static MyGameManager getInstance(){
		return instance;
	}

	// Use this for initialization
	void Start () {
		lives = 3;
		highscore = 0;
		highscoreText.text = 0.ToString ("D5");
		livesText.text = "x " + lives.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
